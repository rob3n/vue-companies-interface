# vue-companies-list

Интерфейс добавления и управления данными компаний с API dadata, для сохранения данных используется localstorage(vuex-persistedstate)

## Установка пакетов

```
yarn install or npm install
```

### Запуск для разработки

```
yarn run serve
```

### Сборка проекта

```
yarn run build
```
