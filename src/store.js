import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    companies: [],
    currentPage: 1,
    perPage: 5
  },
  getters: {
    getCompanies: state => state.companies,
    getCurrentPage: state => state.currentPage,
    getPerPage: state => state.perPage,
    getPageContent: state => {
      const filteredPage = state.companies.filter((el, index) => {
        return (
          index < state.currentPage * state.perPage &&
          index >= (state.currentPage - 1) * state.perPage
        );
      });

      return filteredPage;
    }
  },
  mutations: {
    ADD_COMPANY(state, payload) {
      state.companies.push(payload);
    },
    DELETE_COMPANY(state, payload) {
      state.companies = state.companies.filter(el => el.id !== payload.id);
    },
    EDIT_COMPANY(state, payload) {
      state.companies[payload.id] = {
        id: payload.id,
        name: payload.name,
        director: payload.director
      };
    },
    SET_PAGE(state, pageNumber) {
      state.currentPage = pageNumber;
    }
  },
  actions: {
    addCompanyAction(context, company) {
      context.commit("ADD_COMPANY", company);
    },
    deleteCompanyAction(context, company) {
      context.commit("DELETE_COMPANY", company);
    },
    editCompanyAction(context, company) {
      context.commit("EDIT_COMPANY", company);
    },
    setPageAction(context, pageNumber) {
      context.commit("SET_PAGE", pageNumber);
    }
  },
  plugins: [createPersistedState()]
});
